"use strict";
import Phaser from "phaser";
import Dice from "../../../lib/objects/dice";

export class DiceScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "DiceScene", active: false });
  }

  preload() {
    this.load.spritesheet("dice", "/assets/dices/casino-16x16-10x10.png", {
      frameWidth: 16,
      frameHeight: 16,
    });
  }

  create() {
    this.sum = 0;
    this.scoreText = this.add.text(100, 100, "Demo of Dice", {
      fontSize: "32px",
      fill: "#fff",
    });

    new Dice(this, 150, 200, "dice", 6, 1).setScale(2, 2);
    new Dice(this, 150, 250, "dice", 6, 11).setScale(2, 2);
    new Dice(this, 150, 300, "dice", 6, 21).onFinish(updateText(this));
    new Dice(this, 150, 350, "dice", 6, 31).setScale(3, 3);
    new Dice(this, 150, 400, "dice", 6, 41).onFinish(updateText(this));

    new Dice(this, 250, 200, "dice", 6, 51).setScale(3, 3);
    new Dice(this, 250, 250, "dice", 6, 61).onFinish(updateText(this));
    new Dice(this, 250, 300, "dice", 6, 71).onFinish(updateText(this));
    new Dice(this, 250, 350, "dice", 6, 81).setScale(3, 3);
    new Dice(this, 250, 400, "dice", 6, 91).onFinish(updateText(this));
  }
}

function updateText(scene) {
  return (value) => {
    console.log(scene.sum);
    scene.sum += value;
    scene.scoreText.setText("Demo of Dice with sum: " + scene.sum);
  };
}
