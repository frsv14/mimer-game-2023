let userHP = 100;
let opHP = 100;
let opAttacks = [motspark, motskalla, motslag, motspotta];
let playerMove = 0;

// spelare 1 attacker ->
function spark() {
  
  // Kolla om det är spelarens tur och fortfarande har liv
  if(playerMove == 0 && userHP != 0) {

    // Ger ett heltal mellan 1-10
    let miss = Math.floor((Math.random() * 10) + 1); // chans att missa

    if(miss == 1) {
      // Tilldela text till html
      document.getElementById('message').innerHTML = " spelare 1 attack missade! ";
    }
    else {
      // Tilldela text till html
      document.getElementById('message').innerHTML = " spelare 1 sparkade! "; // attack
      
      // Ger ett heltal mellan 1-10
      let critical = Math.floor((Math.random() * 10) + 1); // critical

      // ja critical
      if(critical == 4){
        // Substrahera 60 från opHP
          opHP -= 60; 
      }
      // nej critical
      else{
        // Substrahera 30 från opHP
        opHP -= 30; 
      }

      // Kolla om karaktären är död
      if(opHP < 0){ 
        opHP = 0
      } 
      
      // Uppdatera apHP i html med opHP
      document.getElementById('apHP').innerHTML = opHP; // nytt hp


      if(opHP == 0){
        // updetarat medelande
        document.getElementById('message').innerHTML = " Motståndare dog! " 
      }
    }

    //vänta
    playerMove = 1; // uppdatera vilken tur det är
}
}

function slag() {
  if(playerMove == 0 && userHP != 0) {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " spelare 1 attack missade! "
  }
  else {
    document.getElementById('message').innerHTML = " spelare 1 slog! ";
    let critical = Math.floor((Math.random() * 10) + 1);
      if(critical == 4){
        for(let x = 0; x < 2; x++){
          opHP = opHP - 20;
        }
      }
      else{
        opHP = opHP - 20;
      }
    if(opHP < 0 ) { opHP = 0}
    document.getElementById('apHP').innerHTML = opHP;
    if(opHP == 0){
      document.getElementById('message').innerHTML = " motståndaren dog! "
    }
  }
  playerMove = 1;
}
}

function skalla() {
  if(playerMove == 0 && userHP != 0) {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " spelare 1 attack missade! "
  }
  else {
    document.getElementById('message').innerHTML = " spelare 1 skallade! ";
    let critical = Math.floor((Math.random() * 10) + 1);
      if(critical == 4){
        for(let x = 0; x < 2; x++){
          opHP = opHP - 10;
        }
      }
      else{
        opHP = opHP - 10;
      }
    if(opHP < 0 ) { opHP = 0}
    document.getElementById('apHP').innerHTML = opHP;
    if(opHP == 0){
      document.getElementById('message').innerHTML = " motståndare dog! "
    }
  }
  playerMove = 1;
}
}
function spotta() {
  if(playerMove == 0 && userHP != 0) {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " spelare 1 attack missade! "
  }
  else {
    document.getElementById('message').innerHTML = " spelare 1 spottade! ";
    let critical = Math.floor((Math.random() * 10) + 1);
      if(critical == 4){
        for(let x = 0; x < 2; x++){
          opHP = opHP - 5;
        }
      }
      else{
        opHP = opHP - 5;
      }
    if(opHP < 0){ opHP = 0}
    document.getElementById('apHP').innerHTML = opHP;
    if(opHP == 0){
      document.getElementById('message').innerHTML = " motståndare dog! "
    }
  }
  //vänta
  playerMove = 1;
}
}



// Motståndare attacker ->

function motspark() {
  let miss = Math.floor((Math.random() * 10) + 1); // chans att missa
  if(miss == 1 ) {
  document.getElementById('message').innerHTML = " motståndare attack missade! " // attack missade
  }
  else {
  document.getElementById('message').innerHTML = " motståndaren sparkade dig!" // attack
    let critical = Math.floor((Math.random() * 10) + 1); // critical
      if(critical == 4){
        for(let x = 0; x < 2; x++){ // critical
          userHP = userHP - 30;
        }
      }
      else{
        userHP = userHP - 30; // inte critical
      }
  if(userHP < 0) { userHP = 0} // dog
  document.getElementById('myHP').innerHTML = userHP; // nytt hp
    if(userHP == 0) { // dör
      document.getElementById('message').innerHTML = "  spelare 1 dog! " // dog
    }
  }
}

function motskalla() {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " motståndarens attack missade! "
  }
  else {
  document.getElementById('message').innerHTML = " motståndaren skallade dig! "
  let critical = Math.floor((Math.random() * 10) + 1);
    if(critical == 4){
      for(let x = 0; x < 2; x++){
        userHP = userHP - 20;
      }
    }
    else{
      userHP = userHP - 20;
    }
  if(userHP < 0) { userHP = 0}
  document.getElementById('myHP').innerHTML = userHP;
    if(userHP == 0){
      document.getElementById('message').innerHTML = " spelare 1 dog! "
    }
  }
}

function motslag() {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " motståndarens attack missade! "
  }
  else {
  document.getElementById('message').innerHTML = " motståndaren slog dig! "
  let critical = Math.floor((Math.random() * 10) + 1);
    if(critical == 4){
      for(let x = 0; x < 2; x++){
        userHP = userHP - 10;
      }
    }
    else{
      userHP = userHP - 10;
    }
  if(userHP < 0) { userHP = 0}
  document.getElementById('myHP').innerHTML = userHP;
    if(userHP == 0){
      document.getElementById('message').innerHTML = " spelare 1 dog! "
    }
  }
}

function motspotta() {
  let miss = Math.floor((Math.random() * 10) + 1);
  if(miss == 1 ) {
    document.getElementById('message').innerHTML = " motståndarens attack missade! "
  }
  else {
  document.getElementById('message').innerHTML = " motstånaren spottade på dig!  "
  let critical = Math.floor((Math.random() * 10) + 1);
    if(critical == 4){
      for(let x = 0; x < 2; x++){
        userHP = userHP - 5;
      }
    }
    else{
      userHP = userHP - 5;
    }
  if(userHP < 0) { userHP = 0}
  document.getElementById('myHP').innerHTML = userHP;
    if(userHP == 0){
      document.getElementById('message').innerHTML = " spelare 1 dog! "
    }
  }
}

//-------

function nästa() { // fortsätt
  if(playerMove == 1 && opHP != 0) { // vems attack
    let move = Math.floor((Math.random() * 4) + 1); // väljer motståndarens attack slumpmässigt
    opAttacks[move](); // kallar en av attackerna från attack arrayn
    playerMove = 0; // updatera spelare attack
  }
}