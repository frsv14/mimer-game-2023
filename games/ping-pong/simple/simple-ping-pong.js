"use strict";
const assetsPath = "../../../node_modules/@mimer/game-assets/";

export class PingPong extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "PingPong", active: false });
    this.gameOn = true;
    this.score = 0;
    this.scoreMsg = "Score: ";
    this.scoreText;
    this.previus = -6;
    this.step = 6;
    this.cursors = null;
    this.playerOne;
    this.playerTwo;
    this.ball;
  }

  preload() {
    this.load.spritesheet(
      "balls",
      assetsPath + "balls/colored-balls-128x128-9.png",
      { frameWidth: 128, frameHeight: 128 }
    );
    this.load.image("paddelOne", assetsPath + "paddles/paddle-blue-256x64.png");
    this.load.image(
      "paddelTwo",
      assetsPath + "paddles/paddle-yellow-256x64.png"
    );
  }

  create() {
    this.ball = this.physics.add.image(150, 200, "balls", 4);
    this.ball.setScale(0.5);
    this.ball.setVelocity(400, 300);
    this.ball.setBounce(1, 1);
    this.ball.setCollideWorldBounds(true);

    this.playerOne = this.physics.add.image(50, 300, "paddelOne");
    this.playerOne.setScale(0.5);
    this.playerOne.setCollideWorldBounds(true);
    this.playerOne.setImmovable(true);
    this.playerTwo = this.physics.add.image(750, 300, "paddelTwo");
    this.playerTwo.setScale(0.5);
    this.playerTwo.setCollideWorldBounds(true);
    this.playerTwo.setImmovable(true);
    this.cursors = this.input.keyboard.createCursorKeys();

    this.scoreText = this.add.text(10, 10, this.scoreMsg + this.score, {
      fontSize: "32px",
      fill: "#FFF",
    });
    this.scoreText.setText(this.scoreMsg + this.score);
  }

  update() {
    if (this.gameOn) {
      this.physics.world.collide(this.ball, this.playerOne);
      this.physics.world.collide(this.ball, this.playerTwo);

      if (this.cursors.up.isDown) {
        this.playerOne.y -= this.step;
      } else if (this.cursors.down.isDown) {
        this.playerOne.y += this.step;
      }

      if (this.cursors.up.isDown) {
        this.playerTwo.y -= this.step;
      } else if (this.cursors.down.isDown) {
        this.playerTwo.y += this.step;
      }

      if (this.ball.x < 50) {
        this.gameOn = false;
        this.ball.setVelocity(0, 0);
        this.add.text(25, 200, "Game Over", {
          fontSize: "128px",
          fill: "#F00",
        });
      }

      if (this.ball.x > 750) {
        this.score++;
        this.scoreText.setText(this.scoreMsg + this.score);
      }

      if (this.score > 10) {
        this.gameOn = false;
        this.ball.setVelocity(0, 0);
        this.add.text(25, 200, "Winner", { fontSize: "128px", fill: "#0F0" });
      }
    }
  }
}
