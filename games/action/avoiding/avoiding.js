"use strict";
import Phaser from "phaser";

class Enemy {
  constructor(graphic, direction) {
    //Bestämmer åt vilket håll enemy ska åka samt position för enemy vilket senare skrivs över
    this.direction = direction;
    this.x = Phaser.Math.Between(0, 800);
    this.y = Phaser.Math.Between(0, 600);

    //Bestämmer position för enemy med hjälp av vilket håll den kommer senare åka
    switch (this.direction) {
      case 0:
        this.x = 900;
        break;
      case 1:
        this.y = -100;
        break;
      case 2:
        this.x = -100;
        break;
      case 3:
        this.y = 700;
        break;
    }

    //Bestämmer radien med random 10-50
    this.r = Phaser.Math.Between(10, 50);
    //Grafiken
    this.graphic = graphic;
    //Hastigheten är random 2-10
    this.speed = Phaser.Math.Between(2, 10);
    //Färgen är också random
    this.color = Math.floor(5 * Math.random());
    //Icke genomskinlig
    this.opacity = 1;
  }

  //Detta körs varje frame
  update() {
    //Skriv ut hållet den åker
    console.log(this.direction);
    //Beroende på vilket håll den åker så tar den antingen bort eller lägger till this.speed till y eller x, beroende på direction
    switch (this.direction) {
      case 0:
        this.x -= this.speed;
        break;
      case 1:
        this.y += this.speed;
        break;
      case 2:
        this.x += this.speed;
        break;
      case 3:
        this.y -= this.speed;
        break;
    }

    //Sätter färgen med hjälp av this.color
    switch (this.color) {
      case 0:
        this.graphic.fillStyle(0x00ff00, 1);
        break;
      case 1:
        this.graphic.fillStyle(0x0000ff, 1);
        break;
      case 2:
        this.graphic.fillStyle(0xffaa00, 1);
        break;
      case 3:
        this.graphic.fillStyle(0xff00ff, 1);
        break;
      case 4:
        this.graphic.fillStyle(0x00ffff, 1);
        break;
    }

    //Ritar ut en cirkel på rätt position med rätt radie
    this.graphic.fillCircle(this.x, this.y, this.r);
  }

  //Om den träffar spelaren
  explode() {
    //Radien ökar med 1 konstant och den blir lite mer genomskinlig hela tiden
    this.r += 1;
    this.opacity -= 0.001;
    this.graphic.fillStyle(0xff0000, this.opacity);
    this.graphic.fillCircle(this.x, this.y, this.r);
  }
}

export class AvoidingScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "AvoidingScene", active: false });
    this.player;
    this.pen;
    this.cursors;
    this.step = 10;
    this.circle = this.gameOn = true;
    this.scoreText;
    this.score = 0;
    this.t = 0;
    this.scoreMsg = "Score: ";
    this.enemies = [];
    this.killer;
    this.victoryMessage;
    this.victoryText;
  }

  //Det som laddas innan spelet börjar
  preload() {
    //Bilden för player
    this.load.image("player", "/assets/ships/nightraider-224x154.png");
  }

  //Detta som skapas i början av spelet
  create() {
    //Player läggs till vilket är en image som preloadades
    this.player = this.add.image(250, 250, "player").setScale(0.25);
    //Grafik läggs till i en variabel som heter pen och fillstyle sätts till 0x555500 med opacity 0
    this.pen = this.add.graphics();
    this.pen.fillStyle(0x555500, 1);
    //Gör det möjligt att kolla vilka pilknappar man trycker
    this.cursors = this.input.keyboard.createCursorKeys();
    //Lägger till en text för score
    this.scoreText = this.add.text(
      this.step,
      this.step,
      this.scoreMsg + this.score,
      { fontSize: "32px", fill: "#fff" }
    );
  }

  //Denna körs för varje frame
  update() {
    if (this.gameOn) {
      //Uppdaterar score och frame samt uppdaterar score text
      this.score++;
      this.t++;
      this.scoreText.setText(this.scoreMsg + this.score);

      //Kollar efter keystrokes
      this.checkKeyStrokes();

      //Uppdaterar spelarens position
      this.player.x = (this.player.x + 800) % 800;
      this.player.y = (this.player.y + 600) % 600;

      //Kollar om en ny fiende behövs spawnas och gör det isåfall
      this.spawnNewEnemy();

      //Clearar grafik variabeln
      this.pen.clear();

      //Loopar igenom alla enemies för att uföra saker med dem
      for (let i = this.enemies.length - 1; i >= 0; i--) {
        //Uppdaterar dem, kollar efter kollisioner med spelaren samt kollar om dem har åkt utanför ramen
        this.enemies[i].update();
        this.checkForEnemyCollisions(i);
        this.checkEnemyOutOfBounds(i);
      }
    }
    //Ifall spelet inte är "on"
    else {
      //Variablen med grafiken clearar
      this.pen.clear();
      //Enemyn som dödade en exploderar
      //this.killer.explode();

      //Konfigurerar victory message
      this.configureVictoryMessage();
      //Skriver ut victory message
      this.victoryText = this.add.text(250, 270, this.victoryMessage, {
        fontSize: "64px",
        fill: "#000",
      });
    }
  }

  //Kollar om piltangenterna är nedtryckta och isåfall rör sig spelaren dit man trycker
  checkKeyStrokes() {
    if (this.cursors.left.isDown) {
      this.player.x -= this.step;
    }
    if (this.cursors.right.isDown) {
      this.player.x += this.step;
    }
    if (this.cursors.up.isDown) {
      this.player.y -= this.step;
    }
    if (this.cursors.down.isDown) {
      this.player.y += this.step;
    }
  }

  //Kollar om en ny fiende ska spawna och spawnar den då
  spawnNewEnemy() {
    if (
      Math.random() < Math.atan(this.t / 5000) / 10 + 0.01 &&
      this.t % 3000 < 2500
    ) {
      //beräknar åt vilket håll den ska röra sig, mellan 0-3
      const direction = Math.floor(Math.random() * 4);
      //pushar en ny enemy till enemies
      this.enemies.push(new Enemy(this.pen, direction));
    }
  }

  //Kollar för kollisioner och isåfall sätter den gameon = false och bestämmer vem som var killer och sätter tint, den sätter också killer radius + 20  och bestämmer storleken på scoretext
  checkForEnemyCollisions(i) {
    if (
      Phaser.Math.Distance.Between(
        this.player.x,
        this.player.y,
        this.enemies[i].x,
        this.enemies[i].y
      ) < this.enemies[i].r
    ) {
      this.player.setTint(0x00ff00);
      this.killer = this.enemies[i];
      this.killer.r += 20;
      this.scoreText.setStyle({ fontSize: "42px", fill: "#0F0" });
      this.gameOn = false;
    }
  }

  //Kollar om en fiende är utanför spel ramen, detta är gjort med en grundläggande if statement och ifall den är utanför så höjs score:et samt den splice:as ut ur arrayn
  checkEnemyOutOfBounds(i) {
    if (
      this.enemies[i].x < -150 ||
      this.enemies[i].x > 950 ||
      this.enemies[i].y < -150 ||
      this.enemies[i].y > 750
    ) {
      this.score += Math.round(
        (this.enemies[i].speed * this.enemies[i].r) / 10
      );
      this.enemies.splice(i, 1);
    }
  }

  //Detta bestämmer vad victory message blir beroende på vilket score man fick
  configureVictoryMessage() {
    if (this.score > 10000) {
      this.player.rotation += 0.1;
    }

    if (this.score > 20000) {
      this.victoryMessage = "AMAZING!";
    } else if (this.score > 15000) {
      this.victoryMessage = "Astounding";
    } else if (this.score > 10000) {
      this.victoryMessage = "Incredible";
    } else if (this.score > 5000) {
      this.victoryMessage = "Well done";
    } else {
      this.victoryMessage = "You can do better!";
    }
  }
}
