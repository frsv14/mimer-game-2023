"use strict";
import Phaser from "phaser";

export class MyScene extends Phaser.Scene {
  constructor(config) {
    super(config);
    Phaser.Scene.call(this, { key: "MyScene", active: false });
    this.player;
    this.pen;
    this.cursors;
    this.step = 10;
    this.circle = {
      x: 250 + Phaser.Math.Between(-10, 10) * this.step,
      y: 250 + Phaser.Math.Between(-10, 10) * this.step,
      r: this.step,
    };
    this.gameOn = true;
    this.scoreText;
    this.score = 0;
    this.scoreMsg = "Score: ";
  }

  preload() {
    this.load.image("player", "/assets/ships/nightraider-224x154.png");
  }

  create() {
    this.player = this.add.image(250, 250, "player").setScale(0.25);
    this.pen = this.add.graphics();
    this.pen.fillStyle(0xff6655, 1);
    this.pen.fillCircle(this.circle.x, this.circle.y, this.circle.r);
    this.cursors = this.input.keyboard.createCursorKeys();
    this.scoreText = this.add.text(
      this.step,
      this.step,
      this.scoreMsg + this.score,
      { fontSize: "32px", fill: "#000" }
    );
  }

  update() {
    if (this.gameOn) {
      if (this.cursors.left.isDown) {
        this.score++;
        this.scoreText.setText(this.scoreMsg + this.score);
        this.player.x -= this.step;
      } else if (this.cursors.right.isDown) {
        this.score++;
        this.scoreText.setText(this.scoreMsg + this.score);
        this.player.x += this.step;
      } else if (this.cursors.up.isDown) {
        this.score++;
        this.scoreText.setText(this.scoreMsg + this.score);
        this.player.y -= this.step;
      } else if (this.cursors.down.isDown) {
        this.score++;
        this.scoreText.setText(this.scoreMsg + this.score);
        this.player.y += this.step;
      }

      if (
        Phaser.Math.Distance.Between(
          this.player.x,
          this.player.y,
          this.circle.x,
          this.circle.y
        ) < this.step
      ) {
        this.pen.clear();
        this.player.setTint(0x00ff00);
        this.scoreText.setStyle({ fontSize: "42px", fill: "#0F0" });
        this.gameOn = false;
      }
    }
  }
}
