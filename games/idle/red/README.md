# Idle/Clicker game

```
Ett enkelt spel där spelaren utför enkla åtgärder som att klicka på skärmen upprepade gånger för att få poäng.
```

## Hur går spelet till?

```
Spelaren trycker på röda knappen upprepade gånger. Efter att man har nått ett viss antal poäng kommer spelet automatiskt höja poängtalet.
```

## Copyright © 2020-2021 ,All rights reserved.
