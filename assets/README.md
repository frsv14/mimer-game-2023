# Images

All images are from [opengameart](https://opengameart.org) or licens is given.

## Alias

- [african-mask.svg](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0
- [cheshire-at](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0
- [cyclop](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0
- [frankenstein](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0
- [gypsy](https://opengameart.org/content/avatar-pack-by-bullseye) by Bull's-Eye
  Graphic Design under CC-BY-SA 3.0
- [pirate](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0
- [queen-of-hearts](https://opengameart.org/content/avatar-pack-by-bullseye) by
  Bull's-Eye Graphic Design under CC-BY-SA 3.0

## Ships

- [nightraider.png](https://opengameart.org/content/night-raider) by dravenx
- [tm-220x120-7x1.png](https://opengameart.org/content/space-ships-side-scroller)
  by TheSource4Life under CC0
- [raptor-128x129.png](https://opengameart.org/content/some-2d-space-shipser) by
  karkarotto under CC0
- [raven-128x128.png](https://opengameart.org/content/some-2d-space-shipser) by
  karkarotto under CC0
- [rhino-128x128.png](https://opengameart.org/content/some-2d-space-shipser) by
  karkarotto under CC0
- [shuttle-128x128.png](https://opengameart.org/content/some-2d-space-shipser)
  by karkarotto under CC0

## Icons

- [arrow-in-circle.png](https://opengameart.org/content/restart-button) by andi
- [number-block.png](https://opengameart.org/content/mushrooms-set-01) by
  GameArtForge

## Numbers

- [number-block.png](https://opengameart.org/content/numbers-blocks-set-01) by
  GameArtForge

## Balls

- [colored-balls-128x128-9.png](https://opengameart.org/content/ball-set-svg) by
  Smileymensch

## Particel

- [many-colors-400x400-9x1.png](https://opengameart.org/content/simple-colored-particle-lights)
  by pauliuw.

## Dice

- [casino-16x16.png](https://opengameart.org/content/dice-collection-d6-d10-16x16px)
  by Refeuh under OGA-BY 3.0

## Background

- [beach-800x600.png](https://commons.wikimedia.org/wiki/File:Boulders_Beach,_South_Africa.jpg)
  by jeffgreenca under CC public domain

## Mixed pictures

- [keyboard-772x329.png](https://commons.wikimedia.org/wiki/File:Cherry_keyboard_105_keys.jpg)
  by 32bitmaschine under public domain

## Cards

- [basic-playing-card-300x420-14x4.png](https://opengameart.org/content/playing-cards)
  by Sharm

## RPG

- [heroine-160x120-13x1.png](https://opengameart.org/content/first-person-dungeon-crawl-protagonist)
  by Redshrike under CC BY-SA 3.0

## Sound

### Laser

- [laser-\*.wav](https://opengameart.org/content/dark-moon-inc-stuff) by Mumu
  under CC-BY 3.0
