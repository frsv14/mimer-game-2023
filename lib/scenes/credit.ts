import Phaser from "phaser";
import Button from "../objects/button";

export class CreditsScene extends Phaser.Scene {
  config: any;

  constructor(config: any) {
    if (!config) {
      config = {};
    }

    if (!config.key) {
      config.key = "creditScene";
    }

    if (!config.nextScene) {
      console.error(config.key + ": Missing next scene");
      return;
    }

    if (!config.time) {
      config.time = 5000;
    }

    super(config);
    this.config = config;
  }

  preload() {
    this.load.spritesheet("ok_button", "/assets/buttons/ok-50x50-3x1.png", {
      frameWidth: 50,
      frameHeight: 50,
    });
  }

  create() {
    console.log("Create Credits scene");
    this.add.text(25, 100, "Credits", {
      fontFamily: "Arial",
      fontSize: 64,
      color: "#ffffff",
    });

    let ok = new Button(this, 80, 400, "ok_button");

    ok.on("pointerdown", this.onOk, this);
  }

  onOk() {
    console.log("Start scene " + this.config.nextScene);
    this.scene.start(this.config.nextScene);
  }
}
