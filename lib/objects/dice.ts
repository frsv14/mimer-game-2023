"use strict";
import { builtinModules } from "module";
import Phaser from "phaser";

export default class Dice extends Phaser.GameObjects.Sprite {
  key: string;
  faces: number;
  start: number;
  value: number = NaN;
  finish?: (value: number) => void;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    key: string,
    faces: number = 6,
    start: number = 0
  ) {
    // TODO: Shopuld check if key is a texture in scene with rith numbers of frames

    super(scene, x, y, key, start + faces - 1);
    this.key = key;
    this.faces = faces;
    this.start = start;

    scene.add.existing(this);
    this.setInteractive();
    this.on("pointerdown", this.onDown, this);
    this.on("pointerup", this.onOver, this);
    this.on("pointerover", this.onOver, this);
    this.on("pointerout", this.onUp, this);
  }

  roll() {
    this.value = Math.floor(Math.random() * this.faces) + 1;
    this.setTexture(this.key, this.start + this.value - 1);
    if (this.finish) {
      this.finish(this.value);
    }
  }

  onFinish(finish: (value: number) => void) {
    this.finish = finish;
  }

  private onDown() {
    this.roll();
  }

  private onOver() {
    this.setTint(0xaaaaaa);
  }

  private onUp() {
    this.clearTint();
  }
}
