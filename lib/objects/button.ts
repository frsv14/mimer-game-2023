"use strict";
import { builtinModules } from "module";
// Inspired from https://phasergames.com/how-to-make-buttons-in-phaser-3/
// @ts-ignore
import Phaser from "phaser";

export default class Button extends Phaser.GameObjects.Sprite {
  key: string;

  constructor(
    scene: Phaser.Scene,
    x: number,
    y: number,
    key: string,
    style?: any
  ) {
    // TODO: Shopuld chec if key is a texture in scene with three frames

    super(scene, x, y, key, 0);
    this.key = key;

    scene.add.existing(this);
    this.setInteractive();
    this.on("pointerdown", this.onDown, this);
    this.on("pointerup", this.onOver, this);
    this.on("pointerover", this.onOver, this);
    this.on("pointerout", this.onUp, this);
  }

  onDown() {
    this.setTexture(this.key, 2);
  }

  onOver() {
    this.setTexture(this.key, 1);
  }

  onUp() {
    this.setTexture(this.key, 0);
  }
}
